import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App.vue'
import router from './router'
import store from './store'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import PortalVue from 'portal-vue'



// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VueAxios, axios)

Vue.use(PortalVue)

/*LOCAL*/
// axios.defaults.baseURL = "http://localhost:3000"; 

/* PRODUCCIÓN */
 axios.defaults.baseURL = "https://back-americana.herokuapp.com";



Vue.config.productionTip = false  

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
